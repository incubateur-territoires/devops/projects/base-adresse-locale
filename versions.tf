terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "~> 16.3.0"
    }
    scaleway = {
      source  = "scaleway/scaleway"
      version = "~> 2.28.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.5.1"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.23.0"
    }
    grafana = {
      source  = "grafana/grafana"
      version = "~> 1.27.0"
    }
  }
  required_version = "~> 1.7"
}
