resource "scaleway_object_bucket" "demo_api_depot" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-demo-api-depot"
  versioning {
    enabled = true
  }
}

resource "scaleway_object_bucket" "dev_api_depot" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-dev-api-depot"
  versioning {
    enabled = true
  }
}
resource "scaleway_object_bucket" "dev_moissonneur" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-dev-moissonneur"
  versioning {
    enabled = true
  }
}

resource "scaleway_object_bucket" "prod_api_depot" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-prod-api-depot"
  versioning {
    enabled = true
  }
}
resource "scaleway_object_bucket" "prod_moissonneur" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-prod-moissonneur"
  versioning {
    enabled = true
  }
}
resource "scaleway_object_bucket" "prod_db_backups" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-prod-db-backups"
}

resource "scaleway_object_bucket" "prod_lab" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-prod-lab"
}
resource "scaleway_object_bucket" "prod_blasons_communes" {
  provider = scaleway.scaleway_project
  name     = "${var.project_slug}-prod-blasons-communes"
}
resource "scaleway_object_bucket_acl" "prod_blasons_communes" {
  provider = scaleway.scaleway_project
  bucket   = scaleway_object_bucket.prod_blasons_communes.id
  acl      = "public-read"
}

resource "scaleway_object_bucket_policy" "permissions" {
  for_each = toset([
    scaleway_object_bucket.demo_api_depot.name,
    scaleway_object_bucket.dev_api_depot.name,
    scaleway_object_bucket.dev_moissonneur.name,
    scaleway_object_bucket.prod_api_depot.name,
    scaleway_object_bucket.prod_moissonneur.name,
  ])
  provider = scaleway.scaleway_project
  bucket   = each.value
  policy = jsonencode({
    Version = "2023-04-17",
    Id      = "MyBucketPolicy",
    Statement = [
      {
        Sid       = "Grant all rights to admins to administer bucket and its policy"
        Effect    = "Allow"
        Action    = ["s3:*"]
        Principal = var.bucket_policy_admin_principal
        Resource = [
          each.value,
          "${each.value}/*",
        ]
      },
      {
        Sid    = "Grant rights to create objects to application"
        Effect = "Allow"
        Action = sort([
          "s3:GetBucketAcl",
          "s3:GetBucketCORS",
          "s3:GetBucketLocation",
          "s3:GetBucketObjectLockConfiguration",
          "s3:GetBucketTagging",
          "s3:GetBucketVersioning",
          "s3:GetBucketWebsite",
          "s3:GetLifecycleConfiguration",
          "s3:ListBucket",
          "s3:ListBucketMultipartUploads",
          "s3:ListBucketVersions",

          "s3:AbortMultipartUpload",
          "s3:GetObject",
          "s3:GetObjectAcl",
          "s3:GetObjectLegalHold",
          "s3:GetObjectRetention",
          "s3:GetObjectTagging",
          "s3:GetObjectVersion",
          "s3:GetObjectVersionTagging",
          "s3:ListMultipartUploadParts",
          "s3:PutObject",
          "s3:PutObjectTagging",
          "s3:PutObjectVersionTagging",
        ])
        Principal = var.bucket_policy_app_principal
        Resource = [
          each.value,
          "${each.value}/*",
        ]
      },
      {
        Sid    = "Grant rights to read objects to developers"
        Effect = "Allow"
        Action = sort([
          "s3:GetBucketAcl",
          "s3:GetBucketCORS",
          "s3:GetBucketLocation",
          "s3:GetBucketObjectLockConfiguration",
          "s3:GetBucketTagging",
          "s3:GetBucketVersioning",
          "s3:GetBucketWebsite",
          "s3:GetLifecycleConfiguration",
          "s3:ListBucket",
          "s3:ListBucketMultipartUploads",
          "s3:ListBucketVersions",

          "s3:GetObject",
          "s3:GetObjectAcl",
          "s3:GetObjectLegalHold",
          "s3:GetObjectRetention",
          "s3:GetObjectTagging",
          "s3:GetObjectVersion",
          "s3:GetObjectVersionTagging",
          "s3:ListMultipartUploadParts",
        ])
        Principal = var.bucket_policy_dev_principal
        Resource = [
          each.value,
          "${each.value}/*",
        ]
      },
    ]
  })
}
